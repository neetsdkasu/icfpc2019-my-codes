@pushd "%~dp0src"

fsc ^
    --nologo ^
    --out:"..\Main.exe" ^
    --target:exe ^
    --debug+ ^
    --debug:full ^
    --warnaserror+ ^
    --codepage:65001 ^
    Result.fs ^
    Prob.fs ^
    Sol.fs ^
    Data.fs ^
    Sim.fs ^
    Solver.fs ^
    Vis.fs ^
    Main.fs

@popd