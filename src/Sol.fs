module Sol


type Cmd =
    | Up = 'W'
    | Left = 'A'
    | Down = 'S'
    | Right = 'D'
    | RotClk = 'E'
    | RotCnt = 'Q'
    | Nothing = 'Z'
    | Extension = 'B'
    | Drill = 'L'
    | Fast = 'F'
    | Reset = 'R'
    | Teleport = 'T'
    | Clone = 'C'


let inline isSingle c =
    match c with
    | Cmd.Extension
    | Cmd.Teleport -> false
    | _ -> true


type Command =
    | Op of char
    | OpPos of char * int * int
    override self.ToString() =
        match self with
        | Op op -> sprintf "%c" op
        | OpPos (op, x, y) -> sprintf "%c(%d,%d)" op x y


type Solution() =
    let mutable commands: Command list array = [|[]|]
    let mutable times = [|0|]
    let get i = Array.get times i
    let add i d = Array.set times i (get i + d)
    member self.Commands = commands
    member self.Times = times
    member self.Time = Array.max times
    member self.Append(i, cmd) =
        cmd :: Array.get commands i
        |> Array.set commands i
        add i 1
    member self.Remove(i) =
        match Array.get commands i with
        | (_::tail) ->
            Array.set commands i tail
            add i (-1)
        | _ -> ()
    member self.AddBot(i) =
        commands <- Array.append commands [|[]|]
        times <- Array.append times [|get i|]
    override self.ToString() =
        commands
        |> Array.map (List.fold (fun a c -> string c + a) "")
        |> String.concat "#"


let inline toStr (sol: Solution) = sol.ToString()

let parse (text: string) =
    let sol = new Solution()
    let mutable p = 0
    let mutable c = '*'
    let mutable x = 0
    let mutable y = 0
    let mutable t = 0
    let mutable rs: (int * int) list = []
    for i in 0 .. String.length text - 1 do
        match text.[i] with
        | '#' ->
            rs <- List.sort rs
            let (tt, pp) = List.item p rs
            sol.AddBot(pp)
            t <- tt
            p <- p + 1

        | '(' ->
            sol.Remove(p)
            y <- 0
        | ',' ->
            x <- y
            y <- 0
        | ')' ->
            sol.Append(p, OpPos(c, x, y))
        | d when '0' <= d && d <= '9' ->
            y <- y * 10 + int d - int '0'
        | ch ->
            sol.Append(p, Op(ch))
            c <- ch
            t <- t + 1
            if ch = 'C' then
                rs <- (t, p) :: rs
    sol


