module Main

open Result

let private showUsage() =
    eprintfn "usage:"
    eprintfn "    Main.exe [-vis] <desc file>"


let private argVis = "-vis"
let private argFile = "-file"


let private parseArgVis (args, m) =
    match args with
    | (vis::tail) when vis = argVis -> Ok((tail, Map.add argVis "" m))
    | _ -> Ok((args, m))


let private parseArgFilepath (args, m) =
    match args with
    | (filepath::[]) -> Ok(([], Map.add argFile filepath m))
    | [] -> Ok(([], m))
    | _ -> Err("invalid arguments")


let private parseArgs args =
    (Array.toList args, Map.empty)
    |> parseArgVis
    |> bind parseArgFilepath
    |> bind (Ok << snd)


let private showCLICommands() =
    printfn "command: help, bye, quit, exit, list <num>, solve <num>, min"


let private parseNum command i =
    if i < 0 || Array.length command <= i then
        Err("invalid command")
    else
        let num = ref 0
        if System.Int32.TryParse(Array.get command i, num) then
            Ok(!num)
        else
            Err("not number")


let private showList files command =
    parseNum command 1
    |> bind (fun num ->
        let fs = Map.find Data.ProbFiles files
        if num < 1 || Array.length fs < num then
            Err("invalid number")
        else
            for i in num .. min (num + 10) (Array.length fs) do
                let df = Array.get fs (i - 1)
                let nm = Data.name df
                let ex = if Data.exists df then "*" else ""
                printfn "%s %s" nm ex
            Ok(())
    )
    |> iterErr (fun err ->
        printfn "%A" err
    )


let private solve files command =
    parseNum command 1
    |> bind (fun num1 ->
        let num2 =
            match parseNum command 2 with
            | Ok (num2) -> num2
            | Err _ -> num1
        if num1 < num2 then
            Ok((num1, num2))
        else
            Ok((num2, num1))
    )
    |> bind (fun (num1, num2) ->
        let fs = Map.find Data.ProbFiles files
        if num1 < 1 || Array.length fs < num1 then
            Err("invalid number")
        elif num2 < 1 || Array.length fs < num2 then
            Err("invalid number")
        else
            seq { num1 .. num2 }
            |> Seq.map (fun i -> (i, Array.get fs (i - 1)))
            |> Seq.fold (fun ret (i, file) ->
                bind (fun _ ->
                    printfn "solve ... %d" i
                    res {
                        let! text = Data.readSrc file
                        let prob = Prob.parse text
                        let sim = Solver.solve prob
                        printfn "time: %A" <| Sim.time sim
                        if Sim.time sim > 0 then
                            let sol = Sim.solution sim
                            Data.writeDst file sol
                    }
                ) ret
            ) (Ok(()))
    )
    |> iterErr (fun err ->
        printfn "%A" err
    )


let private showMin files =
    let fs = Map.find Data.ProbFiles files
    let mutable sel = Array.length fs + 1
    for i in 0 .. Array.length fs - 1 do
        let file = Array.get fs i
        if not (Data.exists file) && i < sel then
            sel <- i + 1
    printfn "min is %d" sel


let rec private cli files =
    printf "> "
    let command = stdin.ReadLine().Trim().ToLower().Split(' ')
    let fin =
        match Array.get command 0 with
        | "bye" | "quit" | "exit" -> true
        | _ -> false
    if fin then
        ()
    else
        match Array.get command 0 with
        | "help" -> showCLICommands()
        | "list" -> showList files command
        | "solve" -> solve files command
        | "min" -> showMin files
        | _ -> printfn "unknown command"
        cli files


let private startCLI() =
    let files = Data.listup()
    showCLICommands()
    cli files


[<EntryPoint>]
let main args =
    res {
        let! m = parseArgs args
        let path = Map.tryFind argFile m
        let vis = Map.containsKey argVis m
        match path with
        | None ->
            if vis then
                Vis.show()
            else
                startCLI()
        | Some path ->
            let! file = Data.datafile path
            let! text = Data.readSrc file
            let problem = Prob.parse text
            Prob.print stderr problem
            if vis then
                Vis.showProblem problem
            else
                Solver.solve problem
                |> ignore
    }
    |> iterErr (fun err ->
        eprintfn "%A" err
        showUsage()
    )
    0