module Sim

let EnumToValue = Microsoft.FSharp.Core.LanguagePrimitives.EnumToValue


type Field =
    | Floor = 0
    | Wall = 1
    | Obstacle = 2
    | Marked = 4


type To =
    | Up = 1
    | Left = 2
    | Down = 4
    | Right = 8


let getNext move x y =
    match move with
    | To.Up -> (x, y - 1)
    | To.Left -> (x - 1, y)
    | To.Down -> (x, y + 1)
    | To.Right -> (x + 1, y)
    | _ -> failwith "Unknown move"


let toCmd move =
    match move with
    | To.Up -> Sol.Cmd.Up
    | To.Left -> Sol.Cmd.Left
    | To.Down -> Sol.Cmd.Down
    | To.Right -> Sol.Cmd.Right
    | _ -> failwith "Unknown move"


let private read p =
    let field = Array.zeroCreate (Prob.height p * Prob.width p)
    let transX x = x - Prob.left p
    let transY y = Prob.bottom p - y
    let indexer = fun x y -> (transY y) * Prob.width p + (transX x)
    for y in Prob.bottom p .. -1 .. Prob.top p do
        for x in Prob.left p .. Prob.right p do
            let v = Prob.get p x y
            Array.set field (indexer x y) v
    (field, transX, transY)


type Bot(x0, y0, time0) =
    let mutable x = x0
    let mutable y = y0
    let mutable time = time0
    let mutable drill = 0
    let mutable fast = 0
    let mutable manipulator = [|(1, 0); (1, 1); (1, -1)|]
    member self.X = x
    member self.Y = y
    member self.Manipulator = manipulator
    member self.Time = time
    member self.Drill = time < drill
    member self.Fast = time < fast
    member self.Done() = time <- time + 1
    member self.SetDrill() = drill <- time + 30
    member self.SetFast() = fast <- time + 50
    member self.Teleport(tx, ty) =
        x <- tx
        y <- ty
    member self.Move(move) =
        let (tx, ty) = getNext move x y
        x <- tx
        y <- ty
    member self.Attach(pos) =
        manipulator <- Array.append manipulator [|pos|]
    member self.Rotate(clockwise) =
        for i in 0 .. Array.length manipulator - 1 do
            let (x, y) = Array.get manipulator i
            let pos = if clockwise then (y, -x) else (-y, x)
            Array.set manipulator i pos


let inline isFloorValue p = (p &&& 3) = int Field.Floor
let inline isNotMarked p = (p &&& int Field.Marked) = 0

type Sim(problem) as self =
    let (field, transX, transY) = read problem
    let width = Prob.width problem
    let height = Prob.height problem
    let indexer x y = y * width + x
    let get x y = Array.get field (indexer x y)
    let set x y v = Array.set field (indexer x y) v
    let bots =
        let startX = transX <| Prob.startX problem
        let startY = transY <| Prob.startY problem
        ref [|new Bot(startX, startY, 0)|]
    let mutable botCount = 1
    let mutable remains = 0
    let sol = new Sol.Solution()
    do
        self.Mark(0)
        remains <- Seq.filter ((=) 0) field |> Seq.length
    member self.Width = width
    member self.Height = height
    member self.Remains = remains
    member self.Solution = sol
    member self.Get(x, y) = get x y
    member self.IsFloor(x, y) = isFloorValue <| get x y
    member self.BotCount = botCount
    member self.Bot(i) = Array.get !bots i
    member self.Time =
        seq { 0 .. botCount - 1 }
        |> Seq.map (fun i -> self.Bot(i).Time)
        |> Seq.max
    member self.InBound(x, y) = 0 <= x && x < width && 0 <= y && y < height
    member self.Mark(i) =
        let bot = self.Bot(i)
        let v = get bot.X bot.Y
        set bot.X bot.Y (v ^^^ (v &&& 3) ||| int Field.Marked)
        for (dx, dy) in bot.Manipulator do
            let (x, y) = (bot.X + dx, bot.Y + dy)
            if self.InBound(x, y) then
                let v = self.Get(x, y)
                if isFloorValue v && isNotMarked v then
                    remains <- remains - 1
                    set x y (v ||| int Field.Marked)
    member self.CanMoveBot(i, move) =
        let bot = self.Bot(i)
        let (tx, ty) = getNext move bot.X bot.Y
        self.InBound(tx, ty) &&
        (bot.Drill || self.IsFloor(tx, ty)) &&
        (not bot.Fast ||
            let (tx, ty) = getNext move tx ty
            self.InBound(tx, ty) &&
            (bot.Drill || self.IsFloor(tx, ty))
        )
    member self.MoveBot(i, move) =
        let bot = self.Bot(i)
        bot.Move(move)
        self.Mark(i)
        if bot.Fast then
            bot.Move(move)
            self.Mark(i)
        bot.Done()
        sol.Append(i, Sol.Command.Op(EnumToValue <| toCmd move))



let inline width (s: Sim) = s.Width
let inline height (s: Sim) = s.Height
let inline get (s: Sim) x y = s.Get(x, y)
let inline botCount (s: Sim) = s.BotCount
let inline bot (s: Sim) i = s.Bot(i)
let inline inBound (s: Sim) x y = s.InBound(x, y)
let inline isFloor (s: Sim) x y = s.IsFloor(x, y)
let inline remains (s: Sim) = s.Remains
let inline solution (s: Sim) = s.Solution
let inline time (s: Sim) = s.Time

let inline make problem = new Sim(problem)

let inline canMoveBot (s: Sim) i move = s.CanMoveBot(i, move)
let inline moveBot (s: Sim) i move = s.MoveBot(i, move)
