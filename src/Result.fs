module Result

type Result<'S,'F> =
    | Ok of 'S
    | Err of 'F


let inline bind func result =
    match result with
    | Ok s -> func s
    | Err e -> Err(e)


let inline isOk r =
    match r with
    | Ok _ -> true
    | Err _ -> false


let inline isErr r = not (isOk r)


let inline get r =
    match r with
    | Ok v -> v
    | Err e -> failwith (sprintf "failed Result.get: %s" e)


let inline iterErr func result =
    match result with
    | Err e -> func e
    | _ -> ()



type ResultBuilder () =
    member this.Bind(result, func) = bind func result
    member this.Combine(result1, result2) =
        if isErr result1 then result1 else result2
    member this.Delay(f) = f()
    member this.Return(value) = Ok(value)
    member this.Zero() = Ok(())


let res = new ResultBuilder()

