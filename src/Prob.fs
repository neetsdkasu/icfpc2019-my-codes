module Prob

type Problem(field, bound, spos, indexer, boosters) =
    let (left, top, right, bottom) = bound
    let (sx, sy) = spos
    let width = right - left + 1
    let height = bottom - top + 1
    member self.Get(x, y) = Array.get field (indexer x y)
    member self.Left = left
    member self.Top = top
    member self.Right = right
    member self.Bottom = bottom
    member self.Width = width
    member self.Height = height
    member self.StartX = sx
    member self.StartY = sy
    member self.Boosters = boosters


let inline left (p: Problem) = p.Left
let inline top (p: Problem) = p.Top
let inline right (p: Problem) = p.Right
let inline bottom (p: Problem) = p.Bottom
let inline height (p: Problem) = p.Height
let inline width (p: Problem) = p.Width
let inline get (p: Problem) x y = p.Get(x, y)
let inline startX (p: Problem) = p.StartX
let inline startY (p: Problem) = p.StartY
let inline boosters (p: Problem) = p.Boosters


let print out problem =
    fprintfn out "width: %d" <| width problem
    fprintfn out "height: %d" <| height problem
    fprintfn out "left: %d" <| left problem
    fprintfn out "top: %d" <| top problem
    fprintfn out "right: %d" <| right problem
    fprintfn out "bottom: %d" <| bottom problem
    for y in bottom problem .. -1 .. top problem do
        for x in left problem .. right problem do
            let find = Array.tryFind (fun (_, bx, by) -> bx = x && by = y)
            match find <| boosters problem with
            | Some((t, _, _)) -> fprintf out "%c" t
            | None ->
                if startX problem = x && startY problem = y then
                    fprintf out "@"
                else
                    fprintf out "%d" <| get problem x y
        fprintfn out ""


let private splitParse (sep: char) f (text: string) =
    text.Split(sep) |> Array.filter ((<>) "") |> Array.map f


let private parsePos (text: string) =
    text.Replace("(", "").Replace(")", "")
    |> splitParse ',' int
    |> Array.chunkBySize 2
    |> Array.map (Array.get >> fun f -> (f 0, f 1))


let private getSep ps =
    let get = Array.get ps
    seq { 0 .. Array.length ps - 1 }
    |> Seq.map (fun i ->
        let a = get i
        let b = get ((i + 1) % Array.length ps)
        if fst a < fst b then (a, b) else (b, a)
    )
    |> Seq.filter (fun (a, b) -> snd a = snd b)
    |> Seq.toArray


let private fill field (left, top, right, bottom) indexer sep f0 fv =
    for x in left .. right do
        let mutable f = f0 * fv
        for y in bottom  .. -1 .. top do
            let index = indexer x y
            let v = f ^^^ Array.get field index
            Array.set field index v
            let e = (Array.exists (fun ((x1, y1), (x2, _)) ->
                y = y1 && x1 <= x && x < x2
            ) sep)
            f <- if e then fv ^^^ f else f


let private checkRegion ps =
    let get = Array.get ps
    seq { 1 .. Array.length ps - 1 }
    |> Seq.forall (fun i ->
        let a = get (i - 1)
        let b = get i
        fst a = fst b || snd a = snd b
    )


let private getSize ps =
    Array.fold (fun (left, top, right, bottom) (x, y) ->
        (min left x, min top y, max right x, max bottom y)
    ) (1000000, 1000000, -1000000, -1000000) ps


let private makeField (left, top, right, bottom) =
    let width = right - left + 1
    let height = bottom - top + 1
    let field = Array.zeroCreate (width * height)
    let indexer = fun x y -> (y - top) * width + (x - left)
    (field, indexer)


let private parseBooster (text: string) =
    let t = text.[0]
    let ss = text.Substring(2, text.Length - 3) |> splitParse ',' int |> Array.get
    (t, ss 0, ss 1)


let parse (text: string) =
    let divs = text.Trim().Split('#') |> Array.get
    let region = divs 0 |> parsePos
    let start = divs 1 |> parsePos |> Array.get <| 0
    let obstacles = divs 2 |> splitParse ';' parsePos
    let boosters = divs 3 |> splitParse ';' parseBooster
    let size = getSize region
    let (field, indexer) = makeField size
    let filler = fill field size indexer
    filler (getSep region) 1 1
    Array.iter (fun obs -> filler (getSep obs) 0 2) obstacles
    new Problem(field, size, start, indexer, boosters)
