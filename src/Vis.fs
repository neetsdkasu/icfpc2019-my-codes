module Vis


type private Application       = System.Windows.Forms.Application
type private DockStyle         = System.Windows.Forms.DockStyle
type private FlowDirection     = System.Windows.Forms.FlowDirection
type private FlowLayoutPanel   = System.Windows.Forms.FlowLayoutPanel
type private Form              = System.Windows.Forms.Form
type private FormStartPosition = System.Windows.Forms.FormStartPosition
type private Label             = System.Windows.Forms.Label
type private PictureBox        = System.Windows.Forms.PictureBox
type private Timer             = System.Windows.Forms.Timer

type private Bitmap      = System.Drawing.Bitmap
type private Color       = System.Drawing.Color
type private Graphics    = System.Drawing.Graphics
type private Point       = System.Drawing.Point
type private Size        = System.Drawing.Size
type private SolidBrush  = System.Drawing.SolidBrush


let mutable Scale = 10

let private draw (picbox: PictureBox) (bitmap: Bitmap) sim =
    use brWhite = new SolidBrush(Color.White)
    use brBlack = new SolidBrush(Color.Black)
    use brGray = new SolidBrush(Color.Gray)
    use brSandyBrown = new SolidBrush(Color.SandyBrown)
    use brRed = new SolidBrush(Color.Red)
    use brYellow = new SolidBrush(Color.Yellow)
    use g = Graphics.FromImage(bitmap)
    for y in 0 .. Sim.height sim - 1 do
        for x in 0 .. Sim.width sim - 1 do
            let f = Sim.get sim x y
            let br =
                match f &&& 7 with
                | 0 -> brWhite
                | 1 -> brBlack
                | 2 -> brGray
                | 4 -> brYellow
                | _ -> brRed
            let px = x * Scale
            let py = y * Scale
            g.FillRectangle(br, px, py, Scale, Scale)
    for i in 0 .. Sim.botCount sim - 1 do
        let bot = Sim.bot sim i
        for (dx, dy) in bot.Manipulator do
            let (x, y) = (bot.X + dx, bot.Y + dy)
            if Sim.inBound sim x y && Sim.isFloor sim x y then
                let px = x * Scale
                let py = y * Scale
                g.FillRectangle(brSandyBrown, px, py, Scale, Scale)
    for i in 0 .. Sim.botCount sim - 1 do
        let bot = Sim.bot sim i
        let px = bot.X * Scale
        let py = bot.Y * Scale
        g.FillRectangle(brSandyBrown, px, py, Scale, Scale)
        g.FillEllipse(brRed, px, py, Scale, Scale)
    picbox.Refresh()

type private Form1(sim, ww, wh) as this =
    inherit Form(ClientSize = new Size(ww, wh),
                 StartPosition = FormStartPosition.CenterScreen,
                 Text = "Draw")
    let components = new System.ComponentModel.Container()
    let timer1 = new Timer(components,
                           Interval = 200)
    let flowlp = new FlowLayoutPanel(Dock = DockStyle.Fill,
                                     FlowDirection = FlowDirection.TopDown)
    let bitmap = new Bitmap(Sim.width sim * Scale, Sim.height sim * Scale)
    let picbox = new PictureBox(Image = bitmap,
                                Size = bitmap.Size)
    let label  = new Label()
    do
        flowlp.Controls.Add(label)
        flowlp.Controls.Add(picbox)
        this.Controls.Add(flowlp)
        Event.add this.timer1_Timer timer1.Tick
        draw picbox bitmap sim
        label.Text <- sprintf "%d" <| Sim.remains sim
    member this.timer1_Timer _ =
        ()
        // let time0 = System.Environment.TickCount
        // draw picbox bitmap
        // let time1 = System.Environment.TickCount
        // let diff  = time1 - time0
        // label.Text <- sprintf "%d" diff
    override this.OnLoad(e) =
        base.OnLoad(e)
        // timer1.Start()
    override this.Dispose(disposing) =
        if disposing then
            match components with
            | null -> ()
            | _    -> components.Dispose()
        base.Dispose(disposing)



let showProblem problem =
    let sim = Sim.make problem
    let sz = max (Sim.width sim) (Sim.height sim)
    Scale <- max 3 (600 / sz)
    let ww = Sim.width sim * Scale + 50
    let wh = Sim.height sim * Scale + 50
    for move in Seq.init 20 (fun i -> Array.get [|Sim.To.Right; Sim.To.Up|] (i % 2)) do
        if Sim.canMoveBot sim 0 move then
            Sim.moveBot sim 0 move
    Application.Run(new Form1(sim, ww, wh))


let show() =
    ()