module Solver

type To = Sim.To

let tos = [|To.Up; To.Left; To.Down; To.Right|]
let ToAll = Array.map int tos |> Array.reduce (|||)
let Visited = 16

let rec private seek sim arr pos indexer =
    if Seq.isEmpty pos then
        None
    else
        let me = Set.minElement pos
        let mutable pos = Set.remove me pos
        let (d, x, y) = me
        if Sim.isNotMarked <| Sim.get sim x y then
            Some((x, y))
        else
            for move in tos do
                let (nx, ny) = Sim.getNext move x y
                if Sim.inBound sim nx ny && Sim.isFloor sim nx ny then
                    let i = indexer nx ny
                    if Array.get arr i = 0 then
                        pos <- Set.add (d + 1, nx, ny) pos
                        Array.set arr i (Visited ||| int move)
            seek sim arr pos indexer


let rec private loop sim indexer =
    let bot = Sim.bot sim 0
    let arr = Array.zeroCreate (Sim.width sim * Sim.height sim)
    Array.set arr (indexer bot.X bot.Y) (Visited ||| ToAll)
    let mutable pos = Set.ofList [(0, bot.X, bot.Y)]

    match seek sim arr pos indexer with
    | None -> ()
    | Some (x, y) ->
        Seq.unfold (fun (x, y) ->
            let i = indexer x y
            let v = Array.get arr i
            Array.set arr i 0
            if v = (Visited ||| ToAll) then
                None
            elif v = 0 then
                failwith "what?"
            else
                let move = Visited ^^^ v
                let demove = ((move <<< 2) ||| (move >>> 2)) &&& ToAll
                let (px, py) = Sim.getNext (enum demove) x y
                Some((move, (px, py)))
        ) (x, y)
        |> Seq.toArray
        |> Array.rev
        |> Array.iter (fun move ->
            Sim.moveBot sim 0 (enum move)
        )
        loop sim indexer


let solve problem =
    let sim = Sim.make problem
    let width = Sim.width sim
    let indexer x y = y * width + x

    loop sim indexer

    sim