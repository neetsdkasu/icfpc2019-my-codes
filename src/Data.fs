module Data

open System.IO
open System.Text.RegularExpressions

open Result

let ProbFiles = "prob"
let OtherFiles = "other"

let private cd = Directory.GetCurrentDirectory()
let private out = Path.Combine(cd, "out")
let private regex = new Regex(@"^prob-\d{3}$")


type DataFile(src, dst, prob) =
    let mutable exists = File.Exists(dst)
    member self.Src = src
    member self.Dst = dst
    member self.Exists = exists
    member self.IsProblem = prob
    member self.Saved() = exists <- File.Exists(dst)


let inline src (df: DataFile) = df.Src
let inline name (df: DataFile) = Path.GetFileNameWithoutExtension(df.Src)
let inline dst (df: DataFile) = df.Dst
let inline exists (df: DataFile) = df.Exists
let inline isProblem (df: DataFile) = df.IsProblem
let inline saved (df: DataFile) = df.Saved()

let inline private readText filepath =
    if File.Exists(filepath) then
        Ok(File.ReadAllText(filepath).Trim())
    else
        Err(sprintf "not found file: %s" filepath)


let inline readSrc df = readText (src df)
let inline readDst df = readText (dst df)

let inline writeDst df sol =
    File.WriteAllText(dst df, Sol.toStr sol)
    saved df


let datafile src =
    if not (File.Exists(src)) then
        Err(sprintf "not found file: %s" src)
    elif Path.GetExtension(src) <> ".desc" then
        Err(sprintf "file extension is not desc: %s" src)
    else
        let src = Path.GetFullPath(src)
        let name = Path.GetFileNameWithoutExtension(src)
        let (dir, prob) =
            if regex.IsMatch(name) then
                (out, true)
            else
                (Path.GetDirectoryName(src), false)
        let dst = Path.Combine(dir, name + ".sol")
        Ok(new DataFile(src, dst, prob))


let listup() =
    Directory.EnumerateFiles(cd, "*.desc", SearchOption.AllDirectories)
    |> Seq.map datafile
    |> Seq.filter isOk
    |> Seq.map get
    |> Seq.groupBy (fun df -> if df.IsProblem then ProbFiles else OtherFiles)
    |> Seq.map (fun (k, s) -> (k, Seq.toArray s))
    |> Map.ofSeq
