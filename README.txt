ICFPContest 2019 My Codes  
-------------------------

Author  : Leonardone @ NEETSDKASU  
License : MIT  
Language: F# 4.0  


## Development Environment  

### Computer  
OS  : Windows 7 Starter SP1 (32-bit)  
CPU : Intel Atom N570 1.66 GHz  
MEM : 1.0 GB  

### Tools  
F#  : Microsoft (R) F# Compiler Version 14.0.23413.0  



### Compile
```
compile.bat
```

### Run
start interactive  
```
Main.exe
```
and   
type this commad   
```
> solve 1 300
```
start auto solving  



### Approach

search empty cell with BFS.  
go to the empty cell.  
loop it.





### 感想

実装力の足りなさに加えて
時間も足りなかったため
幅優先探索(BFS)で空きセルを見つけだしてそこへ向かうの繰り返ししか出来なかった…
ビジュアライザも完成せず…無念
